# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.5.0](https://gitlab.com/bingcoder/publish-pkg/compare/v1.4.0...v1.5.0) (2022-03-01)


### ✨ Features | 新功能

* 修改ci配置3 ([501a17e](https://gitlab.com/bingcoder/publish-pkg/commit/501a17e6c181f16441d95934fa5cb9179cbd08bd))

## [1.4.0](https://gitlab.com/bingcoder/publish-pkg/compare/v1.3.0...v1.4.0) (2022-03-01)


### ✨ Features | 新功能

* 修改ci配置2 ([a097d26](https://gitlab.com/bingcoder/publish-pkg/commit/a097d2602cd8969ad3442d6e95891da4bb911073))

## [1.3.0](https://gitlab.com/bingcoder/publish-pkg/compare/v1.2.0...v1.3.0) (2022-03-01)


### ✨ Features | 新功能

* 修改ci配置 ([5c15005](https://gitlab.com/bingcoder/publish-pkg/commit/5c15005689b515467ba940707e34f61f2d17a088))

## [1.2.0](https://gitlab.com/bingcoder/publish-pkg/compare/v1.1.0...v1.2.0) (2022-03-01)


### ✨ Features | 新功能

* 功能a ([d036042](https://gitlab.com/bingcoder/publish-pkg/commit/d0360429e30e782a30e17c44f3d3bc2e3a2a7b33))


### 🐛 Bug Fixes | Bug 修复

* 修复bug ([b36404d](https://gitlab.com/bingcoder/publish-pkg/commit/b36404da2adb54bc9276e7dbf7698788109e5f8c))

## [1.1.0](///compare/v1.0.0...v1.1.0) (2022-01-27)


### ✨ Features | 新功能

* **MODULE A:** 这是模块a的修改1 e531f1c
* **MODULE A:** 这是模块a的修改2 2486651


### 🐛 Bug Fixes | Bug 修复

* **MODULE A:** 这是模块a的修复 c0daac4

## 1.0.0 (2022-01-27)


### ✨ Features | 新功能

* **MODULE A:** 这是模块a的修改 d03a97f
